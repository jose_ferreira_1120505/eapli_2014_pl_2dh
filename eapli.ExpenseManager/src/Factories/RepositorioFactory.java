/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Factories;

import persistence.inmemory.MonthPageRepository;
import persistence.inmemory.RepositorioLivro;
import persistence.inmemory.RepositorioTipoDespesa;
import persistence.inmemory.IncomeTypeRepositoryImpl;
import persistence.jpa.RepositorioTipoDespesaJPA;

/**
 *
 * @author Grupo8
 */
/**
 * Does some thing in old style.
 *
 * @deprecated use {@link Factory()} instead.  
 */
@Deprecated
public class RepositorioFactory {

    @Deprecated
    public static RepositorioTipoDespesa getRepositorioTipoDespesa() {
        return new RepositorioTipoDespesa();
    }

    @Deprecated
    public static MonthPageRepository getMonthPageRepository() {
        MonthPageRepository repo = new MonthPageRepository();
        return repo;
    }

    @Deprecated
    public static RepositorioLivro getBookRepository() {
        return new RepositorioLivro();
    }

    @Deprecated
    public static IncomeTypeRepositoryImpl getIncomeTypeRepository() {
        return new IncomeTypeRepositoryImpl();
    }

    @Deprecated
    public static RepositorioTipoDespesaJPA getRepositorioTipoDespesaJPA() {
        return new RepositorioTipoDespesaJPA();
    }
}