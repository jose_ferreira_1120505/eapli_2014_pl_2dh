/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package expensemanager;

import Controllers.BookController;
import Controllers.MeanPayController;
import Controllers.RegistarTipoRendimentoController;
import Factories.RepositorioFactory;
import Model.Book;
import Model.Income;
import Model.IncomeType;
import Model.MeanPayment;
import Model.MonthPage;
import Model.TipoDespesa;
import Model.TypePayment;
import Presentation.InicializarSaldoUI;
import Presentation.MainMenu;
import eapli.util.Console;
import eapli.util.Console;
import eapli.util.DateTime;
import java.util.Date;
import persistence.inmemory.IncomeTypeRepositoryImpl;
import persistence.inmemory.MonthPageRepository;
import persistence.inmemory.RepoMeansPayment;
import persistence.inmemory.RepoTypesPayment;
import persistence.inmemory.RepositorioLivro;
import persistence.inmemory.RepositorioTipoDespesa;

/**
 *
 * @author Hugo Costa
 */
public class ExpenseManager {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        BookController bookController = new BookController();
        bookController.validar();
        // bootStrap();
    }

    public static void bootStrap() {
        bootStrapTipoDespesa();
        bootstrapTipoRendimento();
        bootStrapMeansPayment();
        bootstrapMonthPages();
    }

    public static void bootStrapTipoDespesa() {
        TipoDespesa despesa1 = new TipoDespesa("vestuário");
        TipoDespesa despesa2 = new TipoDespesa("lazer");
        TipoDespesa despesa3 = new TipoDespesa("propinas");
        RepositorioTipoDespesa repo = RepositorioFactory.getRepositorioTipoDespesa();
        repo.addlista(despesa1);
        repo.addlista(despesa2);
        repo.addlista(despesa3);

    }

    private static void bootstrapTipoRendimento() {
        IncomeType tipoRendimento1 = new IncomeType("Mesada");
        IncomeType tipoRendimento2 = new IncomeType("Salário");
        IncomeType tipoRendimento3 = new IncomeType("Presentes");
        RepositorioFactory.getIncomeTypeRepository().save(tipoRendimento1);
        RepositorioFactory.getIncomeTypeRepository().save(tipoRendimento2);
        RepositorioFactory.getIncomeTypeRepository().save(tipoRendimento3);
    }

    private static void bootStrapMeansPayment() {
        //Types Payment
        TypePayment typePayment1 = new TypePayment();
        TypePayment typePayment2 = new TypePayment();
        TypePayment typePayment3 = new TypePayment();
        RepoTypesPayment repoTypesPayment = new RepoTypesPayment();
        repoTypesPayment.getTypesPayment().add(typePayment1);
        repoTypesPayment.getTypesPayment().add(typePayment2);
        repoTypesPayment.getTypesPayment().add(typePayment3);

        //MeanPayment
        MeanPayment meanPayment1 = new MeanPayment();
        MeanPayment meanPayment2 = new MeanPayment();
        MeanPayment meanPayment3 = new MeanPayment();
        MeanPayController meanPayController = new MeanPayController();
        meanPayController.getRepoTypesPayment().add(meanPayment1);
        meanPayController.getRepoTypesPayment().add(meanPayment2);
        meanPayController.getRepoTypesPayment().add(meanPayment3);

    }

    public static void bootstrapMonthPages() {
        MonthPage monthpage1 = new MonthPage(2014, 1);
        MonthPage monthpage2 = new MonthPage(2013, 2);
        MonthPage monthpage3 = new MonthPage(2013, 3);
        MonthPage monthpage4 = new MonthPage(2012, 5);

        monthpage1.registerIncome(600, "Salary", new Date(2014, 1, 24), new IncomeType(""));
        monthpage1.registerIncome(400, "Salary", new Date(2014, 1, 22), new IncomeType(""));
        monthpage2.registerIncome(600, "Salary", new Date(2013, 2, 24), new IncomeType(""));
        monthpage3.registerIncome(600, "Salary", new Date(2013, 3, 24), new IncomeType(""));
        monthpage4.registerIncome(600, "Salary", new Date(2012, 5, 24), new IncomeType(""));

        MonthPageRepository repo = RepositorioFactory.getMonthPageRepository();
        repo.saveMonthPage(monthpage1);
        repo.saveMonthPage(monthpage2);
        repo.saveMonthPage(monthpage3);
        repo.saveMonthPage(monthpage4);
    }
}
