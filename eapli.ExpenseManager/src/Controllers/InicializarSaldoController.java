/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.Book;
import Model.MonthPage;
import eapli.util.DateTime;

/**
 *
 * @author JoaoRodrigues
 */
public class InicializarSaldoController {

    private Book _book;
    
    public InicializarSaldoController(Book book) {
         //guardar as referencias dos objectos. (apontadores)
        _book = book;
    }

    public boolean inicializarSaldo(float saldo) {
        //Date dataHoje = new Date();
        //Saldo s = new Saldo(saldo);
        MonthPage mp = new MonthPage(DateTime.currentYear(), DateTime.currentMonth(), saldo);
        return _book.addMonthPage(mp);
    }
    
}
