package Controllers;

import Model.MonthPage;
import persistence.inmemory.MonthPageRepository;
import Factories.RepositorioFactory;
import java.util.ArrayList;

/**
 *
 * @author João Leal
 */
public class ConsultMonthlyIncomeController {

    public ConsultMonthlyIncomeController() {
    }

    public ArrayList<String> ConsultMonthlyIncome(int year) {
        MonthPageRepository monthPageRepository = RepositorioFactory.getMonthPageRepository();
        ArrayList<MonthPage> pages = monthPageRepository.loadMonthPages(year);
        ArrayList<String> income = new ArrayList();
        for (MonthPage mon : pages) {
            income.add(mon.getMonth()+";"+mon.calculateTotalIncome());
        }
        return income;
    }
}
