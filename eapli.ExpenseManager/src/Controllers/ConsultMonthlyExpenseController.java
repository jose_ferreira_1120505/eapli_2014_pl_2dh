/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Factories.RepositorioFactory;
import Model.Expense;
import Model.MonthPage;
import java.util.ArrayList;
import persistence.Factory;
import persistence.IRepositorioMonthPage;
import persistence.IRepositoryFactory;
import persistence.inmemory.MonthPageRepository;

/**
 *
 * @author 1110640 Ricardo Dias
 */
public class ConsultMonthlyExpenseController {
    
    
    /**
     * @param year the year from witch will be loaded all months Expenses
     * @return an ArrayList of strings that contains the "MONTH;%d",EXPENSES 
     */
     public ArrayList<String> ConsultMonthlyExpense(int year) {
        MonthPageRepository monthPageRepository = RepositorioFactory.getMonthPageRepository();
        ArrayList<MonthPage> pages = monthPageRepository.loadMonthPages(year);
        ArrayList<String> expense = new ArrayList();
        for (MonthPage mon : pages) {
            expense.add(mon.getMonth()+";"+mon.calculateTotalExpense());
        }
        return expense;
    }
     /**
      * 
      * @param month The month of the year of witch will be displayed the full expense list
      * @return Returns a Expense list
      */
   public ArrayList<Expense>  ConsultMonthExpenses(int month){
   
   Factory a=Factory.getInstance();
   IRepositoryFactory repositoryFactory = a.getRepositoryFactory();
   IRepositorioMonthPage monthPageRepository = repositoryFactory.getMonthPageRepository();
   
   
    ArrayList<MonthPage> paginas = monthPageRepository.loadAll();
    
    for(int i=0;i<paginas.size();i++){
    if(paginas.get(i).getMonth()==month){
    return paginas.get(i).getExpenses();
    }
    
    
    }
   
   return new ArrayList<>();
   }
    
    
}
