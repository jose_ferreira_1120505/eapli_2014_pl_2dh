/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author Hugo Costa
 */
public class RegistarTipoDespesaUI {
    
    private String nome; 
     

    public RegistarTipoDespesaUI(String nome) {
        this.nome = nome;        
        TipoDespesaController p= new TipoDespesaController(); 
        p.registarTipoDespesa(nome);
    }

    
    public String show() {
        return "RegistarTipoDespesaUI{" + "nome=" + nome + '}';
    }  
    
}
