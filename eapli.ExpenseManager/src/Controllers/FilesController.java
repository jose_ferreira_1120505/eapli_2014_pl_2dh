/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Factories.RepositorioFactory;
import Model.Book;
import Model.Expense;
import Model.Income;
import Model.IncomeType;
import Model.MonthPage;
import Model.TipoDespesa;
import Presentation.FileExportImportUI;
import Presentation.FileExportImportUI.movement_type;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;

import persistence.Factory;
import persistence.IRepositorioMonthPage;
import persistence.IRepositoryFactory;
import persistence.inmemory.RepositorioLivro;

/**
 *
 * @author Ricardo Dias 1110640
 */
public class FilesController {

    public FilesController() {
    }

    /**
     * @param name The filename with the extension already set ("blahblah.csv")
     * @param exporttype The type of data to export (Expense/Income/All) that is
 defined in "Presentation.FileExportImportUI.movement_type"
     * @param start_date The start date of the data to export
     * @param end_date The end date of the data to export
     */
    public void ExportCSV(String name, movement_type exporttype, Date start_date, Date end_date) {
        //Load of the data to export from the Repository
       Book temp =  new Book();
        Factory instance = Factory.getInstance();
        IRepositoryFactory repositoryFactory = instance.getRepositoryFactory();
        
        IRepositorioMonthPage monthPageRepository = repositoryFactory.getMonthPageRepository();
        monthPageRepository.loadAll();
        
       
        //    System.out.println("Do Files controler encontrou :"+temp.getListMonthPages().size());
        
        //ArrayList<MonthPage> aux = temp.loadMonthPages(start_date, end_date);
      //   System.out.println("numero de páginas encontradas: "+aux.size()+" ");
        //Now we need to filter for what we want to export (Everything/Income/Expense)
        if (exporttype == FileExportImportUI.movement_type.Expense) {
           // for (int i = 0; i < aux.size(); i++) {
                //aux.get(i).setIncomes(null);
            }
       // } else if (exporttype == FileExportImportUI.movement_type.Income) {
            //for (int i = 0; i < aux.size(); i++) {
              //  aux.get(i).setExpenses(null);
            }
        }//no need to test for ALL
        //try {  //open file for writing
//            Formatter x = new Formatter(name, "UTF8");
//            System.out.print("\nCreating CSV File...");
//           
//            for (int i = 0; i < aux.size(); i++) {
//                System.out.print("\nExporting page " + i + " out of " + aux.size() + ".");
//                x.format("%d;%d\n", aux.get(i).getYear(), aux.get(i).getMonth());
//                if (aux.get(i).getIncomes() != null && !aux.get(i).getIncomes().isEmpty()) {
//                    //first line of incomes is a default config type ("Income;")
//                    x.format("Income;\n");
//                    for (int a = 0; a < aux.get(i).getIncomes().size(); a++) {
//                        Income inc = aux.get(i).getIncomes().get(a);
//                        if (inc.Datebetween(start_date, end_date)) {
//                            //the output of the date is in the long format
//                            x.format("%f;%s;%d;%s\n", inc.getIncomeValue(), inc.getTypeIncome().toString(), inc.getDate().getTime(), inc.getDescription());
//                        }
//                    }
//                }
//                if (aux.get(i).getExpenses() != null && !aux.get(i).getExpenses().isEmpty()) {
//                    //first line of expenses is a default config type ("Expense;")
//                    x.format("Expense;\n");
//                    for (int a = 0; a < aux.get(i).getExpenses().size(); a++) {
//                        Expense esp = aux.get(i).getExpenses().get(a);
//                        if (esp.Datebetween(start_date, end_date)) {
//                            //the output of the date is in the long format
//                            x.format("%f;%d;%s;%s\n", esp.getPrice(), esp.getDate().getTime(), esp.getDescription(),esp.gettypeExpense().getTipo());
//                        }
//                    }
//                }
//            }
//            x.close();
//        } catch (Exception e) {
//            System.out.print("\nError!\nFailed to Export the Data to the File: " + name + "!");
//        }
//    }
//
//
//    public void ImportCSV(String name) {
//        Book temp =  RepositorioFactory.getBookRepository().loadBook();
//         ArrayList<MonthPage> pages = new ArrayList();
//        try {
//            Scanner fin = new Scanner(new File(name), "UTF8");
//            String str;
//           
//            int line_count = 0;
//                boolean income = false;
//                boolean expense = false;
//        MonthPage current_monthpage = new MonthPage(0, 0);//dummy will never be used if nothing fails!
//                current_monthpage.setSaldo(0);
//            while (fin.hasNextLine()) {
//                line_count++;
//
//                String txt_line = fin.nextLine();
//                String arguments[];
//                arguments = txt_line.split(";");
//
//                switch (arguments.length) {
//                    case 1://only cases are "Income" or "Expense"
//                        if (arguments[0].equalsIgnoreCase("Income")) {
//                            income = true;
//                            expense = false;
//                        }
//                        if (arguments[0].equalsIgnoreCase("Expense")) {
//                            expense = true;
//                            income = false;
//                        }
//                        break;
//                    case 2://only case is when a new monthpage pops up
//                        int year;
//                        int month;
//                        try {
//                            year = Integer.parseInt(arguments[0]);
//                            month = Integer.parseInt(arguments[1]);
//                           
//                            current_monthpage = new MonthPage(year, month);
//                            current_monthpage.setSaldo(0);
//                             pages.add(current_monthpage);
//                           
//                          
//                income = false;
//                expense = false;
//                        } catch (NumberFormatException e) {
//                            System.out.printf("\nBad new monthpage number in line: %d \nIn the file: %s!", line_count, name);
//                        }
//                        break;
//                    case 4://Can only only be an Income or Expense
//                        if (expense) {
//                            try {
//                                 arguments[0]=arguments[0].replace(',','.');
//                                float ammount = Float.parseFloat(arguments[0]);
//                                Date data;
//
//                                data = new Date(Long.parseLong(arguments[1]));
//                                current_monthpage.getExpenses().add(
//                               // current_monthpage.registerExpense(
//                                        new Expense(data, ammount, arguments[2],new TipoDespesa(arguments[3])));
//
//
//                            } catch (NumberFormatException e) {
//                                System.out.printf("\nBad new Expense, was expecting a number and found:%s \nIn line: %d ,in the file: %s!", txt_line, line_count, name);
//                            }
//
//                        }
//                        if (income) {
//                            try {
//                                arguments[0]=arguments[0].replace(',','.');
//                                
//                                float ammount = Float.parseFloat(arguments[0]);
//                                Date data;
//                                data = new Date(Long.parseLong(arguments[2]));
//                                
//                                current_monthpage.getIncomes().add(
//                               // current_monthpage.addIncome(
//                                        new Income(ammount, arguments[1], data, new IncomeType(arguments[3])));
//                            } catch (NumberFormatException e) {
//                                System.out.printf("\nBad new Income, was expecting a number and found:%s \nIn line: %d ,in the file: %s!", txt_line, line_count, name);
//                            }
//                        } else {
//                            System.out.printf("A bad config line was found on line number: %d\nWas expecting a income!", txt_line);
//                        }
//                        break;
//                    default:
//                        break;
//                }
//            }
//          
//            fin.close();
//            System.out.print("Numero de páginas lidas: "+pages.size()+"");
//           
//        } catch (Exception e) {
//            if (e instanceof FileNotFoundException) {
//                System.out.print("\nError! The File: " + name + " was not found!");
//            }
//        }        
//    }
//
// 
//
// 
//    /**
//     * 
//     * Export movements data to XML format
//     * @param name Name of the file
//     * @param exporttype Export type specification (i.e Export, Import or All movements)
//     * @param start_date Start date of movements wanted to export
//     * @param end_date End date of movements wanted to export
//     * 
//     */
//    public void ExportXML(String name, movement_type exporttype, Date start_date, Date end_date){
//        //Load of the data to export from the Repository
//        Book livroGeral = RepositorioLivro.loadBook();
//        //ArrayList<MonthPage> monthPagesList = livroGeral.loadMonthPages(start_date, end_date);
//        ArrayList<MonthPage> monthPagesList = livroGeral.getListMonthPages();
//        //Now we need to filter for what we want to export (Everything/Income/Expense)
//        if (exporttype == FileExportImportUI.movement_type.Expense) {
//            for (int i = 0; i < monthPagesList.size(); i++) {
//                monthPagesList.get(i).setIncomes(null);
//            }
//        } else if (exporttype == FileExportImportUI.movement_type.Income) {
//            for (int i = 0; i < monthPagesList.size(); i++) {
//                monthPagesList.get(i).setExpenses(null);
//            }
//        }//no need to test for ALL
//        try{
//            XMLEncoder outputFile= new XMLEncoder(new BufferedOutputStream(new FileOutputStream(name)));
//            System.out.print("\nCreating XML File...\n");
//            
//            for (int i = 0; i < monthPagesList.size(); i++) {
//                System.out.print("\nExporting page " + i + " out of " + monthPagesList.size() + ".");
//
//                if (monthPagesList.get(i).getIncomes() != null && !monthPagesList.get(i).getIncomes().isEmpty()) {
//                    //first line of incomes is a default config type ("Income;")
//                    for (int a = 0; a < monthPagesList.get(i).getIncomes().size(); a++) {
//                        Income inc = monthPagesList.get(i).getIncomes().get(a);
//                        if (inc.Datebetween(start_date, end_date)) {
//                            //the output of the date is in the long format
//                            outputFile.writeObject(inc);
//                        }
//                    }
//                }
//                if (monthPagesList.get(i).getExpenses() != null && !monthPagesList.get(i).getExpenses().isEmpty()) {
//                    //first line of expenses is a default config type ("Expense;")
//                    for (int a = 0; a < monthPagesList.get(i).getExpenses().size(); a++) {
//                        Expense exp = monthPagesList.get(i).getExpenses().get(a);
//                        if (exp.Datebetween(start_date, end_date)) {
//                            //the output of the date is in the long format
//                            outputFile.writeObject(exp);
//                        }
//                    }
//                }
//            }
//            System.out.println("\nExported Successfully!");
//            outputFile.close();
//        } catch (Exception e) {
//            System.out.println(e);
//            System.out.print("\nError!\nFailed to Export the Data to the File: " + name + "!");
//
//        }
//    }
//    public void ImportXML(String name, movement_type importType){
//        int incomes =0; int expenses=0;
//        try{
//            XMLDecoder inputFile= new XMLDecoder(new BufferedInputStream(new FileInputStream(name)));
//            RegisterIncomeController register_income_controller = new RegisterIncomeController();
//            RegisterExpenseController register_expense_controller = new RegisterExpenseController();
//            try{
//                System.out.println("\nImporting XML File...");
//                if(importType.equals(movement_type.Income)){
//                    for(;;){
//                        Income inc = (Income)inputFile.readObject();
//                        register_income_controller.addIncome(inc);
//                        incomes++;
//                        System.out.println("Imported Income - "+incomes);
//                    }
//                }
//                else if (importType.equals(movement_type.Expense)){
//                    for(;;){
//                        Expense exp = (Expense)inputFile.readObject();
//                        register_expense_controller.registerExpense(exp);
//                        expenses++;
//                        System.out.println("Imported expense - "+expenses);
//                    }
//                }
//                else{
//                    for(;;){
//                        Object obj = inputFile.readObject();
//                        if(obj.getClass().equals(Income.class)){
//                            Income inc = (Income)obj;
//                            register_income_controller.addIncome(inc);
//                            incomes++;
//                            System.out.println("Imported Income - "+incomes);
//                        }
//                        else if(obj.getClass().equals(Expense.class)){
//                            Expense exp = (Expense)obj;
//                            register_expense_controller.registerExpense(exp);
//                            expenses++;
//                            System.out.println("Imported expense - "+expenses);
//                        }
//                        System.out.println(obj.getClass());
//                    }
//                }
//            } catch (ArrayIndexOutOfBoundsException e) {
//                inputFile.close();
//                System.out.println("Imported Successfully!");
//            }
//            
//        }catch(Exception e){
//            System.out.println(e);
//        }
        
    //}
