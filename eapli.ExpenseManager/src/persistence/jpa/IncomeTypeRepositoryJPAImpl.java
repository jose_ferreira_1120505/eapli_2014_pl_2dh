package persistence.jpa;

import Model.IncomeType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import persistence.IIncomeTypeRepository;

/**
 *
 * @author João Fonseca
 */
public class IncomeTypeRepositoryJPAImpl implements IIncomeTypeRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("ExpenseManagerPU");
    EntityManager manager = factory.createEntityManager();

    /**
     * Persists an IncomeType in the database
     * @param tr the IncomeType that is to be persisted in the database
     */
    @Override
    public void save(IncomeType incomeType) {
        manager.getTransaction().begin();
        manager.persist(incomeType);
        manager.getTransaction().commit();

        manager.close();
    }

    /**
     * Loads all the IncomeTypes
     * @return returns an arrayList with all the IncomeTypes persisted in the database
     */
    @Override
    public ArrayList<IncomeType> loadAll() {
        ArrayList<IncomeType> list = new ArrayList<>();
        manager.getTransaction().begin();
        Query query = manager.createQuery("select i from IncomeType i", IncomeType.class);
        list.addAll(query.getResultList());
        manager.close();
        
        return list;
    }

}
