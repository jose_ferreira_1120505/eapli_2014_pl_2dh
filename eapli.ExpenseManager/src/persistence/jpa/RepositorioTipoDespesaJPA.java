/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.jpa;

import Model.IncomeType;
import Model.TipoDespesa;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import persistence.IRepositorioTipoDespesa;

/**
 *
 * @author João Leal
 */
public class RepositorioTipoDespesaJPA implements IRepositorioTipoDespesa {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("ExpenseManagerPU");
    EntityManager manager = factory.createEntityManager();

    @Override
    public void save(TipoDespesa tipoDespesa) {
        manager.getTransaction().begin();
        manager.persist(tipoDespesa);
        manager.getTransaction().commit();

        manager.close();

    }
    
    public List<TipoDespesa> loadAll(){
    List<TipoDespesa> tiposdespesa;
    Query query = manager.createQuery("from TipoDespesa ex");
    tiposdespesa=query.getResultList();
    manager.close();
    return tiposdespesa;
    }
}
