/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.jpa;

import Model.Income;
import javax.persistence.*;
import persistence.IRepositorioIncome;

/**
 *
 * @author ASUS
 */
public class RepositorioIncomeJPA implements IRepositorioIncome{
        EntityManagerFactory factory =Persistence.createEntityManagerFactory("ExpenseManagerPU");
    EntityManager manager=factory .createEntityManager();
    
    @Override
    public void save(Income income){
        manager.getTransaction().begin();
        manager.persist(income);
        manager.getTransaction().commit();
                
    }
}
