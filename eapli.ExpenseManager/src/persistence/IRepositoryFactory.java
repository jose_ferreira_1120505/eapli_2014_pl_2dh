/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistence;

/**
 *
 * @author Tiago
 */
public interface IRepositoryFactory {
 
    public IRepositorioMonthPage getMonthPageRepository();
    public IIncomeTypeRepository getIncomeTypeRepository();
    public IRepositoryMeanPay getRepositoryMeanPay();
    public IRepositorioTipoDespesa getRepositorioTipoDespesa(); 
    
}
