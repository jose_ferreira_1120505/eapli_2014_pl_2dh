/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistence.inmemory;

import persistence.IIncomeTypeRepository;
import persistence.IRepositorioMonthPage;
import persistence.IRepositorioTipoDespesa;
import persistence.IRepositoryFactory;
import persistence.IRepositoryMeanPay;

/**
 *
 * @author Tiago
 */
public class InMemoryRepositoryFactory implements IRepositoryFactory {    
    
    @Override
    public IRepositorioMonthPage getMonthPageRepository(){
        return new RepoMonthPage();
    }

    public RepoMeansPayment getRepoMeansRepository() {
        return new RepoMeansPayment();
    }

    public RepoTypesPayment getRepoTypesRepository() {
        return new RepoTypesPayment();
    }

    public RepositorioLivro getLivroRepository() {
        return new RepositorioLivro();
    }

    public RepositorioTipoDespesa getTipoDespesaRepository() {
        return new RepositorioTipoDespesa();
    }
    
    @Override
    public IIncomeTypeRepository getIncomeTypeRepository() {
        return new IncomeTypeRepositoryImpl();
    }

    @Override
    public IRepositoryMeanPay getRepositoryMeanPay() {
        return new RepoMeansPayment();
    }

    @Override
    public IRepositorioTipoDespesa getRepositorioTipoDespesa() {
        return new RepositorioTipoDespesa(); 
    }

    
}
