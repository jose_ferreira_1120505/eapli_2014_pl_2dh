/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.inmemory;

import Model.TypePayment;
import java.util.ArrayList;

/**
 *
 * @author Pedro Amaral
 */
public class RepoTypesPayment {
    private static ArrayList<TypePayment> repoTypesPay;

    public RepoTypesPayment() {
        repoTypesPay = new ArrayList();
    }
    
    public ArrayList getTypesPayment(){
        return repoTypesPay;
    }
}
