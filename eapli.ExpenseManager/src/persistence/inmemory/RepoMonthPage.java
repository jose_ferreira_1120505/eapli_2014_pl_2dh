/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.inmemory;

import Model.MonthPage;
import java.util.ArrayList;
import persistence.IRepositorioMonthPage;

/**
 *
 * @author JoaoRodrigues
 */
public class RepoMonthPage implements IRepositorioMonthPage{

    private ArrayList<MonthPage> listaMonthPage = new ArrayList<MonthPage>();

    public void save(MonthPage mpr) {
        listaMonthPage.add(mpr);
    }

    public ArrayList<MonthPage> loadAll() {
        return listaMonthPage;
    }
}
