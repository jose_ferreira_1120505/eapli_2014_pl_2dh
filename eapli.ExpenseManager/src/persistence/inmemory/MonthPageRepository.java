/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.inmemory;

import Model.MonthPage;
import java.util.ArrayList;
import java.util.Date;
import persistence.Factory;
import persistence.IRepositorioMonthPage;
import persistence.IRepositoryFactory;

/**
 *
 * @author Grupo8
 */
public class MonthPageRepository {

//private static ArrayList<MonthPage> monthPages = new ArrayList<MonthPage>();
    private ArrayList<MonthPage> monthPages;

    public MonthPageRepository() {
         IRepositoryFactory r = Factory.getInstance().getRepositoryFactory();
         IRepositorioMonthPage irmp = r.getMonthPageRepository();
         monthPages = irmp.loadAll();
    }
    public boolean registerMonthPage(MonthPage mp) {
        try {
            IRepositoryFactory r = Factory.getInstance().getRepositoryFactory();
            IRepositorioMonthPage irmp = r.getMonthPageRepository();
            irmp.save(mp);
            monthPages = irmp.loadAll();
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    public MonthPage loadMonthPage(Date date) {
        for (MonthPage monthPage : monthPages) {
            if (monthPage.verifyMonthPageDate(date)) {
                return monthPage;
            }
        }
        return null;
    }
    
    
    public ArrayList<MonthPage> loadMonthPage(Date start,Date end) {
       ArrayList<MonthPage> aux=new ArrayList();
        for (MonthPage monthPage : monthPages) {
            if (monthPage.Datebetween(start,end)) {
                aux.add(monthPage);
            }
        }
        return aux;
    }
    
    
    
    

    public void saveMonthPage(MonthPage monthPage) {
        for (int i = 0; i < monthPages.size(); i++) {
            if (monthPages.get(i).compare(monthPage)) {
                monthPages.set(i, monthPage);
            }
        }
    }

    public ArrayList<MonthPage> loadMonthPages(int year) {
        ArrayList<MonthPage> pages = new ArrayList();
        for (MonthPage mon : monthPages) {
            if (mon.getYear() == year) {
                pages.add(mon);
            }
        }
        return pages;
    }
    
    public ArrayList<MonthPage> getMonthPages() {
        return monthPages;
    }
}
