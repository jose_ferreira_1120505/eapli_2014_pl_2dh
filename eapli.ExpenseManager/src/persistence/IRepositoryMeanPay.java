/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistence;

import Model.MeanPayment;
import java.util.List;

/**
 *
 * @author 
 */
public interface IRepositoryMeanPay {
    public void save(MeanPayment mp);
    
    public List<MeanPayment> loadAll();
}