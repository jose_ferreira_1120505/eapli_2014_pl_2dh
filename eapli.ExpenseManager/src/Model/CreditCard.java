/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import javax.persistence.Entity;

/**
 *
 * @author Zé
 */
@Entity
public class CreditCard extends MeanPayment {

    private int num;
    private String bank;

    public CreditCard() {
    }

    public CreditCard(int num, String desc, String bank) {
        super();
        typeID = 2;
        this.num = num;
        description = desc;
        this.bank = bank;
    }

    public int getNum() {
        return num;
    }

    public String getBank() {
        return bank;
    }

}