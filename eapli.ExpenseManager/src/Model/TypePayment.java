/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Pedro Amaral
 */
@Entity
public class TypePayment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int Id;
    private typesPayment typesPayment;

    public enum typesPayment {

        Money, DebitCard, CreditCard, Cheque
    };

    public TypePayment() {

    }

    public typesPayment getTypesPayment() {
        return typesPayment;
    }

}
