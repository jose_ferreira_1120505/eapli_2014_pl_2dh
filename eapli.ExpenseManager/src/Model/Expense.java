/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;
import persistence.inmemory.RepositorioLivro;

/**
 *
 * @author ASUS
 */
@Entity
public class Expense implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int Id;
    private float price;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;
    private String description;
    private TipoDespesa typeExpense;
    private Book book;

    public Expense() {
    }

    public Expense(Date date, float price, String description, TipoDespesa typeExpense) {
        this.date = date;
        this.price = price;
        this.description = description;
        this.typeExpense=typeExpense;
    }
    public Expense(Date date, float price, TipoDespesa typeExpense) {
        this.date = new Date(date.getYear(),date.getMonth(),0);
        this.price = price;
        this.description = "Acumulação mensal de despesa";
        this.typeExpense=typeExpense;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price the price payed to set
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date of the transaction to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * @return the Espense Type
     */
    public TipoDespesa gettypeExpense() {
        return typeExpense;
    }

    /**
     * @param description the description of the expense to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param start the start date of a set time interval (Note that the day is
     * ignored)
     * @param end the end date of a set time interval (Note that the day is
     * ignored)
     * @return returns true if this expense line is inside the defined time
     * interval
     */
    public boolean Datebetween(Date start, Date end) {
        //testado e a funcionar corretamente
        Calendar start_c = Calendar.getInstance();
        Calendar end_c = Calendar.getInstance();
        start_c.setTimeInMillis(start.getTime());
        end_c.setTimeInMillis(end.getTime());
        Calendar aux = Calendar.getInstance();
        aux.setTimeInMillis(date.getTime());
        return start_c.getTimeInMillis() <= aux.getTimeInMillis() && aux.getTimeInMillis() <= end_c.getTimeInMillis();
    }
    /**
     * 2 Expenses are considered equal if the type of those expenses are equal
     * @param obj
     * 
     */
    @Override
    public boolean equals(Object obj){
        
        return this.gettypeExpense().getTipo().equals(((Expense)obj).gettypeExpense().getTipo());
    }
    
    
      public String monthGroup (int month){
        String str=""; int aux;
        ArrayList<Expense> pattern = new ArrayList<>();
        for(MonthPage mp : book.getListMonthPages()){
            aux=mp.getMonth(); 
            if(month==aux){
                for(Expense exp : mp.getExpenses()){
                    if(pattern.contains(exp)){
                        float pr=pattern.get(pattern.indexOf(exp)).getPrice();
                        float pr2=pattern.get(pattern.indexOf(exp)).getPrice();
                        pattern.get(pattern.indexOf(exp)).setPrice(pr+pr2);
                    }
                    else{
                        pattern.add(new Expense(new Date(month,exp.getDate().getMonth(),0), exp.getPrice(), exp.gettypeExpense()));
                    }
                }
            }
        }
        float totalAnual = getMonthGroups(month);
        for(int i = 0; i<3 ;i++){
            Expense maior=new Expense();
            maior.setPrice(0);
            for(Expense exp : pattern){
                if(exp.getPrice()>maior.getPrice()){
                    maior=exp;
                }
            }
            float aux_price=maior.getPrice();
            str+=maior.gettypeExpense().getTipo()+" "+aux_price+" "+(aux_price/totalAnual)*100+"% || ";
            pattern.remove(maior);
        }
        return str;
    }
      
        public float getMonthGroups(int ano){
        book=RepositorioLivro.loadBook();
            float total=0; int aux;
        for(MonthPage mp : book.getListMonthPages()){
            aux=mp.getExpenses().get(ano).getDate().getYear();
            if(ano==aux){
                for(Expense exp : mp.getExpenses()){
                    total+=exp.getPrice();
                }
            }
        }
        return total;
    }
    
}
