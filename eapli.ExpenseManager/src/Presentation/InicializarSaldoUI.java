/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Model.Book;
import eapli.util.Console;
import Controllers.InicializarSaldoController;
/**
 *
 * @author JoaoRodrigues
 */
public class InicializarSaldoUI {

    private InicializarSaldoController _initSCntroller;
    private Book _book;
    
    public InicializarSaldoUI(Book book) {
        //guardar as referencias dos objectos. (apontadores)
        _book = book;
        _initSCntroller = new InicializarSaldoController(book);
    }
    
    public void show()
    {
        //ler da consola o saldo que é do tipo float eg:10.30
         float saldo = (float)Console.readDouble("Introduza o Saldo Inicial\n");
         _initSCntroller.inicializarSaldo(saldo);
         //depois de ininicalizar o saldo chama se o MainMenu normalmente
         MainMenu manu = new MainMenu(_book);
         manu.mainLoop();
    }
}