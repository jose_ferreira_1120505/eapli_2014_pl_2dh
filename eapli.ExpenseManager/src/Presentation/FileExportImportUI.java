/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Controllers.FilesController;
import Model.Book;
import Model.MonthPage;
import eapli.util.Console;
import eapli.util.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Ricardo Dias 1110640
 */
public class FileExportImportUI {

    /**
     *
     * The type of movement referring to a bank account
     */
    public enum movement_type {
        Income, Expense, All
    }

    public FileExportImportUI() {
        
        
        int readInteger;
        do {
            printMenu();
            readInteger = Console.readInteger("\tChose an option between 1,4\n->");

        } while (readInteger < 1 || readInteger > 4);
        FilesController temp = new FilesController();
        String name;
        
        switch (readInteger) {
            case 1:
                name=Console.readLine("\tWhat is the name of the file?\n->");
                name=Files.ensureExtension(name,".csv");
                //temp.ImportCSV(name);
                break;
            case 2:
                name=Console.readLine("\tWhat is the name of the file?\n->");
                name=Files.ensureExtension(name,".csv");
                temp.ExportCSV(name, getMovementType(), getStartDate(),getEndDate());
            break;
            case 3:
                name=Console.readLine("\tWhat is the name of the file?\n->");
                name=Files.ensureExtension(name,".xml");
               // temp.ImportXML("name",getMovementType());
                break;
            case 4:
                name=Console.readLine("\tWhat is the name of the file?\n->");
                name=Files.ensureExtension(name,".xml");
                //temp.ExportXML(name, getMovementType(), getStartDate(),getEndDate());
                break;
            default:
                System.out.println("Not valid option");
                break;

        }

    }

    private void printMenu() {
        System.out.println("------------Export\\Import MENU--------------"
                + "\n\t1 - Import CSV"
                + "\n\t2 - Export CSV"
                + "\n\t3 - Import XML"
                + "\n\t4 - Export XML");

    }
        
    

    /**
     * Asks the user what is the file name.
     *
     * @return Retuns the file name that the user has chosen.
     */
    private String getFileName() {
        String aux = "";
        do {
            aux = Console.readLine("\nInsert the file name:\n-> ");
        } while (aux.isEmpty());
        return aux;
    }

    /**
     * Asks the user what is the Account movement type and only exits after a existing type has been selected
     * @return Returns a movement_type (that exists in FileExportImportUI)
     */
    private movement_type getMovementType() {
        do {
            System.out.print("\nWhat kind of movement do you want?\n(Income|Expense|All)\n->");
            Scanner in = new Scanner(System.in);
            String temp = in.nextLine();
            temp=temp.toUpperCase();
            switch (temp) {
                case "INCOME":
                    return movement_type.Income;
                   
                case "EXPENSE":
                    return movement_type.Expense;
                   
                case "ALL":
                    return movement_type.All;
                  
                default:
                    System.out.print("\nPlease insert a valid type!");
                    break;
            }
        } while (true);
    }
    /**
     * 
     *Asks the user a start date 
     */
    private Date getStartDate(){
    Date aux=Console.readDate("\nWhat is the start date?\n->"); 
    return aux;
    }
    
     /**
     * 
     *Asks the user a end date 
     */
    private Date getEndDate(){
    Date aux=Console.readDate("\nWhat is the end date?\n->"); 
    return aux;
    }
    
    
    
    /**
     * 
     * Exports a CSV file, asking the user first for the name of the file, date, and what type to export (Income|Expense|All)
     */
    public void exportCSV(){
    String file=getFileName();
    movement_type type=getMovementType();
    int data_valida=0;
    Date start;
    Date end;
    do{
     start=getStartDate();
     end=getEndDate();
    if(start.before(end)){data_valida=1;}else{
    System.out.print("\nError: End date is before start date!");
    
    }}while(data_valida==0);
    
    
    //Chamada do export CSV do FilesController
    
    
    }
    
    
}












 
