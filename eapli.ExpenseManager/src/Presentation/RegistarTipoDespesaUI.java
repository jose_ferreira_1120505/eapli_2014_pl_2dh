/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Controllers.TipoDespesaController;
import eapli.util.Console;

/**
 *
 * @author Hugo Costa
 */
public class RegistarTipoDespesaUI {
    
    private String nome; 
     

    public  void RegistarTipoDespesaUI(String nome) {
        nome = nome;        
        TipoDespesaController controller= new TipoDespesaController(); 
        controller.registarTipoDespesa(nome);
    }

    
    public void show() {
         nome = Console.readLine("Indique o nome");
         RegistarTipoDespesaUI(nome);     
    }  
    
}
