/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Controllers.RegistarTipoRendimentoController;
import eapli.util.Console;


/**
 *
 * @author i110300
 */
public class RegistarTipoRendimentoUI {
    String nome;
    RegistarTipoRendimentoController trController = new RegistarTipoRendimentoController();
    
    public void show(){
           nome=Console.readLine("Qual o nome?");
           setNomeTipoRendimento(nome);
    }
    
    public void setNomeTipoRendimento(String nome){
        trController.RegistarTipoRendimento(nome);
    }
}
