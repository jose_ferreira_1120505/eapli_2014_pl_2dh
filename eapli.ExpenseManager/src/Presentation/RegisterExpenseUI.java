/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Presentation;

import Controllers.RegisterExpenseController; 
import Model.IncomeType;
import Model.TipoDespesa;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Catarina
 */
public class RegisterExpenseUI {
        public void show() {
            
                    RegisterExpenseController registerExpenseController = new RegisterExpenseController();
                
                   List <TipoDespesa> typeExpense=registerExpenseController.loadListExpenseTypes();
                   if(!typeExpense.isEmpty()){
                    TipoDespesa typeExpenses = showExpenseTypes(typeExpense);                 
                    if (typeExpenses != null) { 
                        String descricao=Console.readLine("Introduza a descrição da despesa:\n");
                            float price=(float)Console.readDouble("Introduza o preço da despesa:\n");
                           
                            Date date = Console.readDate("Data:\n");
                            RegisterExpenseController expensecontroller=new RegisterExpenseController();
                            expensecontroller.registerExpense(descricao,date,price,typeExpenses);
                    }else{
                        System.out.println("Não foi escolhido nenhum tipo de despesa\n Escolha novamente");
                    }
                    }else{
                       System.out.println("Lista de tipos de despesa vazio");    
                   }
    }
        
        
         private TipoDespesa showExpenseTypes(List<TipoDespesa> typeExpense) {
        for (int i = 0; i < typeExpense.size(); i++) {
            System.out.println((i + 1) + " - " + typeExpense.get(i).toString());
        }
        System.out.println("0 - Voltar");
        int op = Console.readInteger("Escolha Tipo de Despesa:\n");
        if (op > 0 && op <= typeExpense.size()) {
            return typeExpense.get(--op);
        } else {
            return null;
        }
    }
}
