
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Model.TipoDespesa;
import persistence.inmemory.MonthPageRepository;
import persistence.inmemory.RepositorioTipoDespesa;
import eapli.util.Console;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Hugo Costa
 */
public class MenuConsultas extends BaseUI{
    
     public void mainConsultas() {
        int option;
        do {
            System.out.println(header("CONSULT MENU"));
            System.out.println("\t1- Consult the monthly incomes of a year");
            System.out.println("\t2- Consult the month balance");
            System.out.println("\t3-Consult the monthly expenses of a year");
            System.out.println("\t0 Exit");

            option = Console.readInteger("Choose an option");

            switch (option) {
                
                case 3:{
                ConsultMonthlyExpenseUI ui = new ConsultMonthlyExpenseUI();
                break;
                }
                
                case 2: {
                ConsultarSaldoMesUI saldomes=new ConsultarSaldoMesUI();
                saldomes.ConsultarSaldoMesUI();
                break;
                
                }
                case 1: {
                    ConsultMonthlyIncomeUI ui = new ConsultMonthlyIncomeUI();
                    ui.consultMonthlyIncome();
                    break;
                }
                case 0: {
                    System.out.println("Exiting");
                    break;
                }
                default: {
                    System.out.println("Invalid option");
                    break;
                }
            }

        } while (option != 0);
    }

    @Override
    public String show() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
