/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Presentation;

import Controllers.ConsultMonthlyExpenseController;
import Model.Expense;
import static eapli.util.Console.readInteger;
import java.util.ArrayList;

/**
 *
 * @author 1110640 Ricardo Dias
 * 
 */
public class ConsultMonthlyExpenseUI {
    
    private static final String[] MONTHS={"January","February","March","April","May","June","July","August","September","October","November","December"};
    
    /**
     * Asks the user what year does he want to know the Monthly expense list
     */
    public ConsultMonthlyExpenseUI() {
    
    }
    
    public void allYear(){
          int year = readInteger("Which year?");
        ConsultMonthlyExpenseController controller = new ConsultMonthlyExpenseController();
        ArrayList<String> pages = controller.ConsultMonthlyExpense(year);
        for (int i = 0; i < pages.size(); i++) {
            String[] aux=pages.get(i).split(";");
            System.out.println(MONTHS[Integer.parseInt(aux[0])-1] + ": " + aux[1]);
        }
    
    }
    
    public void  SingleMonthExpense(){
        int mes=-1;
        do{
        mes=readInteger("Which month?");
        }while(mes<1||mes>12);
         ConsultMonthlyExpenseController controller = new ConsultMonthlyExpenseController();
        ArrayList<Expense> ConsultMonthExpenses = controller.ConsultMonthExpenses(mes);
        System.out.println("Expenses for "+MONTHS[mes]+":");
    for(int i=0;i<ConsultMonthExpenses.size();i++){
        System.out.println("Price: "+
    ConsultMonthExpenses.get(i).getPrice()+" Expense type: "+
    ConsultMonthExpenses.get(i).gettypeExpense().getTipo()+"Date: "+
    ConsultMonthExpenses.get(i).getDate());
    }
        
    }

  
}
