/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Controllers.ConsultarSaldoMesController;
import Model.Book;
import Model.MonthPage;
import persistence.inmemory.MonthPageRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 *
 * @author José
 */
public class ConsultarSaldoMesUI {
   
    
    public void ConsultarSaldoMesUI(){
    Scanner in=new Scanner(System.in);
    ArrayList<MonthPage> paginasMensais=new ArrayList();
    MonthPageRepository repo=new MonthPageRepository();
    int year;
    do{
        System.out.println("Insert the desired year?");
        year=in.nextInt();
        paginasMensais=repo.loadMonthPages(year);
        if(paginasMensais.isEmpty()){
            System.out.println("The year is not available");
        }
    }while(!paginasMensais.isEmpty());
 
    Date datahoje=new Date();
    int mesatual;
    GregorianCalendar cale=new GregorianCalendar();
    mesatual=cale.MONTH; 
    
    int op=0;
    do{
    int x=0;
        System.out.println("Months:\n");
    for(MonthPage pag: paginasMensais){
        x++;
       
        if(pag.getMonth()!=mesatual){
        System.out.println(x+": "+(pag.getMonth()+1)+"/"+ pag.getYear());
        //verificar como vem o mes. se de 0  11 ou normal
        }
 
    
    }
    
    System.out.println("Choose the month : ");
    op=in.nextInt();
    if(op<x||op>x){
        Date dataMonthPage=new Date();
        dataMonthPage.setMonth(paginasMensais.get(op-1).getMonth());
        dataMonthPage.setMonth(paginasMensais.get(op-1).getYear());
        ConsultarSaldoMesController controller = new ConsultarSaldoMesController(dataMonthPage);
        //faz o mês mes escolhido op-1
        System.out.println("Month balance : "+controller.saldoMes+" €");
    }else if(op!=0){
        System.out.println("choose the right option:");
    }
    }
    while(op!=0);
    }

}
