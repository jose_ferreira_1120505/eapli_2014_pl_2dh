/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo Costa
 */
public class TipoDespesaTest {
    private String nameTester = "sapatilhas";
    private TipoDespesa tipodespesa;
    
    public TipoDespesaTest() {
    tipodespesa = new TipoDespesa(nameTester);  
    }
    
    @BeforeClass
    public static void setUpClass() {
    System.out.println("TipoDespesaTest begin"); 
    }
    
    @AfterClass
    public static void tearDownClass() {
    System.out.println("TipoDespesaTest end");
    }
    
    @Before
    public void setUp() {
    for (int i = 0; i < 10; i++) {
            System.out.printf("Test number %d\n", i);
        }
    
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class TipoDespesa.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TipoDespesa instance = new TipoDespesa();
        String expResult = "sapatilhas";
        String result = tipodespesa.toString();
        assertEquals(expResult, result);
    
       
    }

    /**
     * Test of setTipo method, of class TipoDespesa.
     */
    @Test
    public void testSetTipo() {
        System.out.println("setTipo");
        String tipo = "sapatilhas";
        TipoDespesa instance = new TipoDespesa();
        instance.setTipo(tipo);
        
       
    }

    /**
     * Test of getTipo method, of class TipoDespesa.
     */
    @Test
    public void testGetTipo() {
        System.out.println("getTipo");
        TipoDespesa instance = new TipoDespesa();
        String expResult = "sapatilhas"; 
        String result = tipodespesa.getTipo();
        assertEquals(expResult, result);
       
        
    }
}