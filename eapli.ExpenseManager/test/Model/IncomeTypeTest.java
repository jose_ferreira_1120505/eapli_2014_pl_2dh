/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago
 */
public class IncomeTypeTest {
    
    public IncomeTypeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class IncomeType. True case
     */
    @Test
    public void testCompare1() {
        System.out.println("compareTrue");
        IncomeType typeIncome = new IncomeType ("Salário");
        IncomeType instance = new IncomeType ("Salário");
        boolean expResult = true;
        boolean result = instance.compare(typeIncome);
        assertEquals(expResult, result);
    }

    /**
     * Test of compare method, of class IncomeType. False case
     */
    @Test
    public void testCompare2() {
        System.out.println("compareFalse");
        IncomeType typeIncome = new IncomeType ("Salário");
        IncomeType instance = new IncomeType ("Juros");
        boolean expResult = false;
        boolean result = instance.compare(typeIncome);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of toString method, of class IncomeType.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        IncomeType instance = new IncomeType ("Salário");
        String expResult = "Salário";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
