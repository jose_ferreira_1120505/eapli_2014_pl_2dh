/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Model.Book;
import Model.Income;
import Model.IncomeType;
import Model.MonthPage;
import Presentation.FileExportImportUI;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import persistence.inmemory.RepositorioLivro;

/**
 *
 * @author i120034
 */
public class FilesControllerTest {
    
    MonthPage mp= new MonthPage(2014,10,1000);
    Date da1= new Date(114,10,1); Date da2= new Date(114,10,2);
    Date da3= new Date(114,10,3); Date da4= new Date(114,10,4);
    IncomeType ip = new IncomeType("Extras");
    Income ic1 = new Income(100,"Poker",da1,ip);
    Income ic2 = new Income(100,"Poker2",da2,ip);
    Income ic3 = new Income(100,"Poker3",da3,ip);
    Income ic4 = new Income(100,"Poker4",da4,ip);

    public FilesControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of ExportCSV method, of class FilesController.
     */
    @Test
    public void testExportCSV() {
        System.out.println("ExportCSV");
        String name = "";
        FileExportImportUI.movement_type exporttype = null;
        Date start_date = null;
        Date end_date = null;
        FilesController instance = new FilesController();
        instance.ExportCSV(name, exporttype, start_date, end_date);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of ExportXML method, of class FilesController.
     */
    @Test
    public void testExportXML() {
        System.out.println("ExportXML");
        Book aux = RepositorioLivro.loadBook();
        String name = "jose.xml";
        mp.getIncomes().add(ic1); mp.getIncomes().add(ic2);mp.getIncomes().add(ic3);mp.getIncomes().add(ic4);
        FileExportImportUI.movement_type exporttype = FileExportImportUI.movement_type.All;
        Date start_date = new Date(114,9,30); 
        Date end_date = new Date(114,11,1);
        aux.getListMonthPages().add(mp);
        System.out.println(aux.getListMonthPages().size());
        FilesController instance = new FilesController();
        //instance.ExportXML(name, exporttype, start_date, end_date);
        
    }
    @Test
    public void testImportXML() {
        System.out.println("ImportXML");
        Book aux = RepositorioLivro.loadBook();
        String name = "jose.xml";
        FileExportImportUI.movement_type importType = FileExportImportUI.movement_type.All;
        System.out.println(aux.getListMonthPages().size());
        FilesController instance = new FilesController();
       // instance.ImportXML(name, importType);
    }
}
