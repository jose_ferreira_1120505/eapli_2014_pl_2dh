/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Model.Book;
import Model.Expense;
import Model.Income;
import Model.IncomeType;
import Model.MonthPage;
import Model.TipoDespesa;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import persistence.inmemory.RepositorioLivro;

/**
 *
 * @author i120034
 */
public class MonthlyExpensesTendenciesControllerTest {
    MonthPage mp= new MonthPage(2014,10,1000);
    Date da1= new Date(114,10,1);
    TipoDespesa ip = new TipoDespesa("Extra");
    TipoDespesa ip2 = new TipoDespesa("Comida");
    Expense ic1 = new Expense(da1,100,"Poker",ip);
    Expense ic2 = new Expense(da1,100,"Poker",ip);
    Expense ic3 = new Expense(da1,100,"Feijoada",ip2);
    


    /**
     * Test of CalculateTendencies method, of class MonthlyExpensesTendenciesController.
     */
    @Test
    public void testCalculateTendencies() {
        Book aux = RepositorioLivro.loadBook();
        System.out.println("CalculateTendencies");
        MonthlyExpensesTendenciesController instance = new MonthlyExpensesTendenciesController();
        String expResult = "";
        mp.getExpenses().add(ic1);mp.getExpenses().add(ic2);mp.getExpenses().add(ic3);
        mp.setYear(2014);
        aux.getListMonthPages().add(mp);
        String result = instance.CalculateTendencies();
        System.out.println(result);
        assertEquals(expResult, result);
        
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of MonthTendencies method, of class MonthlyExpensesTendenciesController.
     */
//    @Test
//    public void testMonthTendencies() {
//        System.out.println("MonthTendencies");
//        MonthPage mp = null;
//        MonthlyExpensesTendenciesController instance = new MonthlyExpensesTendenciesController();
//        String expResult = "";
//        String result = instance.MonthTendencies(mp);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//
//    }
//
//    /**
//     * Test of anualTendencies method, of class MonthlyExpensesTendenciesController.
//     */
//    @Test
//    public void testAnualTendencies() {
//        System.out.println("anualTendencies");
//        int ano = 0;
//        MonthlyExpensesTendenciesController instance = new MonthlyExpensesTendenciesController();
//        String expResult = "";
//        String result = instance.anualTendencies(ano);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//
//    }
//    
}
